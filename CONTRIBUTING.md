# Contribute

## Installation

Clone the repository and run 

php composer.phar install

## Add or pick a feature

Before you code a feature/bug fix please see if an issue exists or create a new one. Use issue number to your commit
message for <scope>

Do a merge request.

## Commit message

Please have a look to this documentation to create your [commit message](http://karma-runner.github.io/latest/dev/git-commit-msg.html)

**Allowed <type> values**:

- feat: (new feature for the user)
- fix: (bug fix for the user)
- doc: (changes to the documentation)
- style: (formatting, missing semi colons, etc; no production code change)
- refactor: (refactoring production code, eg. renaming a variable)
- test: (adding missing tests, refactoring tests; no production code change)
- chore: (updating composer, .gitingore etc; no production code change)

**Example <scope> values**:

- issue number: pick the issue number and use it to link from commit message eg. (#1)
- config
- composer
- dev-requirements
- etc.

If you are uncertain about the scope or you are coding for global change do not use <scope> on commit message.
**Always** use issue number in first.
